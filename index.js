'use strict'

const express = require('express')
const bodyParser = require('body-parser')
const app = express();
//const server = require(`http`).createServer(app)
const Firestore = require('@google-cloud/firestore')
const db = new Firestore({projectId:'tempno-com',keyFilename:'tempno-admin.json'})
const moment = require('moment')
//const request = require('request')
const fs = require('fs')
const disk = require('diskusage')
const os = require('os')

app.use(bodyParser.urlencoded({extended:true}))
app.use(bodyParser.json())

let start = false
let mapping = {}

app.get('/', (req, res) => {
    res.status(200).send('ok');
});


app.post(`/api`,(req,res)=>{
    res.setHeader('Access-Control-Allow-Origin', '*')
    let post = req.body
    console.log('Post:')
    console.log(post)

    if(!!post.k && !!post.m) {
        let key_pub = encodeURIComponent(req.body.k)
        read_pub(key_pub).then(pub => {
            let mount = pub.mount
            console.log(`found mount`,mount)
            if(!!enc_api[post.m]){
                if(!post.p) post.p = {}
                post.p['k'] = key_pub
                enc_api[post.m](mount,post.p).then(data=>{
                    res.send({success:true,data:data,disk:pub.disk})
                }).catch(e=>{
                    res.send({success:false,error:e})
                })
            }else res.send({success:false,error:'Missing method.'})
        }).catch(e=>{res.send({success:false,error:e})})
    }else{
        res.send({success:false,error:'Not authenticated or missing method.'})
    }
})
let enc_api = {
    note_read: function(mount,params){
        return new Promise((resolve,reject)=>{
            console.log(`Read from folder ${mount}`)

            fs.readdir(`/media/usb/${mount}/n`,(e,files)=>{
                if(e){
                    console.log(`Dir read error:`)
                    console.log(e)
                    reject(e)
                }else{
                    console.log(files)
                    let promises = []
                    let notes = {}
                    files.forEach(file => {
                        promises.push(
                            read_file(`/media/usb/${mount}/n/${file}`).then(data =>{
                                notes[file] = data
                            }).catch(e=>{
                                console.log(`File read error`)
                                console.log(e)
                            })
                        )
                    })
                    Promise.all(promises).then(()=>{
                        resolve({files:notes})
                    })
                }
            })
        })
    },
    note_create: function(mount,params){
        return new Promise((resolve,reject)=>{
            //write to storage device
            console.log('params:')
            console.log(params)
            if(!!params.content && !!params.enc_doc_key){
                console.log(`write to folder ${mount}`)
                let timestamp = moment().format('x')
                fs.writeFile(`/media/usb/${mount}/n/${timestamp}.txt`,JSON.stringify([params.enc_doc_key,params.content,params.sharing]),e=>{
                    if(e){
                        console.log(`File write error:`)
                        console.log(e)
                        reject(e)
                    }else{
                        resolve('file written to drive')
                        //check sharing
                        if(!!params.sharing){
                            console.log(`found a sharing request`)
                            for(let key in params.sharing){
                                let encoded = encodeURIComponent(key)
                                share_create(params.k,encoded,timestamp)
                            }

                        }
                    }
                })
            }else{
                reject('No content.')
            }

        })
    },
    contacts_update: function(mount,params){
        return new Promise((resolve,reject)=>{
            console.log(`update contacts file on: ${mount}`)
            if(!!params.contacts){
                //let string = JSON.stringify(params.contacts)
                fs.writeFile(`/media/usb/${mount}/contacts.txt`,params.contacts,e=>{
                    if(e)reject(e)
                    else enc_api.contacts_read(mount,params).then(contacts=>{
                        resolve(contacts)
                    }).catch(e=>{reject(e)})
                })
            }else reject('missing params')
        })
    },
    contacts_read: function(mount,params){
        return new Promise((resolve,reject)=>{
            console.log(`read contacts file on: ${mount}`)

            fs.readFile(`/media/usb/${mount}/contacts.txt`,(e,buff)=>{
                if(e)reject(e)
                else resolve(buff.toString())
            })

        })
    }
}

function read_file(file){
    return new Promise((resolve,reject)=>{
        fs.readFile(file,(e,buff)=>{
            if(e) reject(e)
            else{
                resolve(decodeURIComponent(buff.toString()))
            }
        })
    })
}

function read_pub(key_pub) {
    return new Promise((resolve, reject) => {
        if(!!mapping[key_pub]){
            //check drive status
            let mount = mapping[key_pub]
            disk.check(`/media/usb/${mount}`,(e,info)=>{
                if(e) reject(e)
                else{
                    resolve({mount:mount,disk:{available:info.available,free:info.free,total:info.total}})
                }
            })
        }else reject('dne')
    })
}

function listen_drives(){
    return new Promise((resolve, reject) => {
        let listener = db.collection(`encdata`).doc(`mapping`).onSnapshot(s=>{
            console.log(`New mapping received.`)
            let obj = s.data()
            console.log(obj.drives)
            mapping = obj.drives
            if(!start){
                start = true
                resolve()
            }
        })
    })
}

function share_create(key_pub_owner,key_pub_guest,timestamp){
    if(!!mapping[key_pub_guest]){
        let guest_mount = mapping[key_pub_guest]
        if(!fs.existsSync(`/media/usb/${guest_mount}/r`)){
            fs.mkdirSync(`/media/usb/${guest_mount}/r`)
        }
        if(!fs.existsSync(`/media/usb/${guest_mount}/r/${key_pub_owner}`)){
            fs.mkdirSync(`/media/usb/${guest_mount}/r/${key_pub_owner}`)
            console.log(`making a directory...`)
        }
        fs.writeFile(`/media/usb/${guest_mount}/r/${key_pub_owner}/${timestamp}`,' ',e=>{
            if(e) console.log(`error sharing`,e)
        })
    }
}

if (module === require.main) {
    //get file locations
    listen_drives().then(()=>{
        // [START server]
        // Start the server
        const server = app.listen(process.env.PORT || 8080, () => {
            const port = server.address().port;
            console.log(`App listening on port ${port}`);
        });
        // [END server]
    })
}
module.exports = app;